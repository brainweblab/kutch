<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-frontend',
            'baseUrl' => '/kutch',
        ],
        'user' => [
            'class' => 'app\components\User',
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => false,
            'rules' => [
                '' => 'site/index',                                   
                '<controller:site|dashboard>/<id:\d+>/<slug:\w+>' => '<controller>/view',
                '<controller:site|dashboard>/<action:\w+>/<id:\d+>/<slug:\w+>' => '<controller>/<action>',
                '<controller:site|dashboard>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:site|dashboard>/<action:\w+>' => '<controller>/<action>',
                '<slug:\w+>/<action:\w+>' => 'user/<action>',
                
            ],
        ],
        
    ],
    'params' => $params,
];
