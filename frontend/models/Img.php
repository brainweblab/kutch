<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "{{%img}}".
 *
 * @property int $id
 * @property int $user_id
 * @property string|null $home_header
 * @property string|null $home_slider1
 * @property string|null $home_slider2
 * @property string|null $about_img
 * @property string|null $gall_1
 * @property string|null $gall_2
 * @property string|null $gall_3
 * @property string|null $gall_4
 * @property string|null $contact_img
 */
class Img extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%img}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id'], 'integer'],
            [['home_header', 'home_slider1', 'home_slider2', 'about_img', 'gall_1', 'gall_2', 'gall_3', 'gall_4', 'contact_img'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'home_header' => 'Home Header',
            'home_slider1' => 'Home Slider1',
            'home_slider2' => 'Home Slider2',
            'about_img' => 'About Img',
            'gall_1' => 'Gall 1',
            'gall_2' => 'Gall 2',
            'gall_3' => 'Gall 3',
            'gall_4' => 'Gall 4',
            'contact_img' => 'Contact Img',
        ];
    }
}
