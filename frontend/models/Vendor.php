<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "{{%vendor}}".
 *
 * @property int $id
 * @property int $user_id
 * @property string $name
 * @property string $company_name
 * @property string $phone
 * @property string $image
 * @property string $phone_alt
 * @property string $email_alt
 * @property string $url
 * @property string $website
 * @property string $facebook
 * @property string $instagram
 * @property string $twitter
 * @property int $status
 * @property int $is_deleted
 * @property string $created_dt
 * @property string $modified_dt
 */
class Vendor extends \yii\db\ActiveRecord
{
    public $email, $home_header;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%vendor}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'name', 'status', 'is_deleted', 'email'], 'required'],
            [['company_name', 'phone', 'url', 'address'], 'required', 'on' => 'update'],
            [['user_id', 'status', 'is_deleted'], 'integer'],
            [['url'],'unique'],
            [['facebook', 'instagram', 'twitter'],'url'],
            [['created_dt', 'modified_dt'], 'safe'],
            [['name', 'company_name', 'phone', 'image', 'phone_alt', 'email_alt', 'url', 'website', 'facebook', 'instagram', 'twitter'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'name' => 'Name',
            'email' => 'Email',
            'company_name' => 'Company Name',
            'phone' => 'Phone',
            'image' => 'Image',
            'phone_alt' => 'Phone Alt',
            'email_alt' => 'Email Alt',
            'url' => 'Custom Url',
            'website' => 'Website',
            'facebook' => 'Facebook',
            'instagram' => 'Instagram',
            'twitter' => 'Twitter',
            'status' => 'Status',
            'is_deleted' => 'Is Deleted',
            'created_dt' => 'Created Dt',
            'modified_dt' => 'Modified Dt',
            'address' => 'Address',
        ];
    }
}
