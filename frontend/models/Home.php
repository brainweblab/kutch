<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "{{%home}}".
 *
 * @property int $id
 * @property int $user_id
 * @property string $short_about
 * @property string $working_hour
 * @property string $created_dt
 * @property string $modified_dt
 */
class Home extends \yii\db\ActiveRecord
{
    public $header, $slider1, $slider2, $mon, $tue, $wed, $thu, $fri, $sat, $sun, $gall1, $gall2, $gall3, $gall4;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%home}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['working_hour'], 'string'],
            [['created_dt', 'modified_dt'], 'safe'],
            [['short_about'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'short_about' => 'Short About',
            'working_hour' => 'Working Hour',
            'created_dt' => 'Created Dt',
            'modified_dt' => 'Modified Dt',
            'mon'=> 'Monday',
            'tue'=> 'Tuesday',
            'wed'=> 'Wednesday',
            'thu'=> 'Thursday',
            'fri'=> 'Friday',
            'sat'=> 'Saturday',
            'sun'=> 'Sunday'
        ];
    }
}
