<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "{{%about}}".
 *
 * @property int $id
 * @property int $user_id
 * @property resource $about
 * @property string $vision
 * @property string $mission
 * @property string $goal
 * @property string $created_dt
 * @property string $modified_dt
 */
class About extends \yii\db\ActiveRecord
{
    Public $about_img, $header;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%about}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [[ 'user_id'], 'required'],
            [['user_id'], 'integer'],
            [['created_dt', 'modified_dt'], 'safe'],
            [['vision', 'mission', 'goal', 'about'], 'string', 'max' => 160],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'about' => 'About',
            'vision' => 'Vision',
            'mission' => 'Mission',
            'goal' => 'Goal',
            'created_dt' => 'Created Dt',
            'modified_dt' => 'Modified Dt',
        ];
    }
}
