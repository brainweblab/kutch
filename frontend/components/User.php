<?php

namespace app\components;

use common\models\Role;
use frontend\models\Vendor;
use Yii;

/**
 * Extended yii\web\User
 *
 * This allows us to do "Yii::$app->user->something" by adding getters
 * like "public function getSomething()"
 *
 * So we can use variables and functions directly in `Yii::$app->user`
 */
class User extends \yii\web\User
{
    public function getUsername()
    {
        return \Yii::$app->user->identity->username;
    }

    public function getName()
    {
        $v = SELF::vendor();
        return $v->company_name;
    }
    
    public function getCusturl(){
        $url = SELF::vendor();
        return $url->url;
    }

    public function getLogo()
    {
        $url = SELF::vendor();
        return $url->image;
    }
    
    public function getRole()
    {
        $role = Role::findOne(['id' => Yii::$app->user->identity->role_id]);
        return $role;
    }

    public function getEmail()
    {
        return \Yii::$app->user->identity->email;
    }

    public function vendor(){
        if(@$_GET['slug']){
            $v = Vendor::find()->Where(['url' => $_GET['slug']])->one();
        }
        else{
            $v = Vendor::find()->where(['user_id' => Yii::$app->user->identity->id])->one();
        }
        
        return $v;
    }
}
