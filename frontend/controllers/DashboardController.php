<?php

namespace frontend\controllers;

use common\models\User;
use frontend\models\About;
use frontend\models\Home;
use frontend\models\Img;
use Yii;
use frontend\models\Vendor;
use yii\db\Query;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\helpers\Url;

/**
 * DashboardController implements the CRUD actions for Vendor model.
 */
class DashboardController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'home', 'profile', 'gallery', 'about', 'file'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],

                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        if (in_array($action->id, ['file', 'view'])) {
            $this->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }

    public function actionIndex()
    {
        $this->view->title = 'Dashboard';
        $id = Yii::$app->user->id;
        $model = Vendor::findOne(['user_id' => $id]);
        $model->scenario = 'update';
        $model->email = Yii::$app->user->email;

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $user = User::findOne($id);;
            $user->email = $model->email;
            if ($user->save() && $model->save()) {
                Yii::$app->session->setFlash('success', 'Profle updated successfully.');
                return $this->redirect(['profile', 'id' => $user->id]);
            } else {
                Yii::$app->session->setFlash('error', 'Somthing went wrong. Please try again later.');
            }
        }

        return $this->render('profile', [
            'model' => $model,
        ]);
    }

    public function actionHome()
    {
        $this->view->title = 'Home Page';
        $id = Yii::$app->user->id;
        $query = new Query();
        $query->select('i.home_header, i.home_slider1, i.home_slider2, i.gall_1, i.gall_2, i.gall_3, i.gall_4, h.short_about, h.working_hour')
            ->from('ks_img as i')
            ->join('LEFT JOIN', 'ks_home as h', "h.user_id = $id")
            ->where(['i.user_id' => $id]);
        $command = $query->createCommand();
        $a = $command->queryOne();

        if ($a) {
            $model = Home::find()->where(['user_id' => $id])->one();
            if (empty($model)) {
                $model = new Home();
            }
            $day = unserialize($a['working_hour']);
            $model->mon = @$day['mon'];
            $model->tue = @$day['tue'];
            $model->wed = @$day['wed'];
            $model->thu = @$day['thu'];
            $model->fri = @$day['fri'];
            $model->sat = @$day['sat'];
            $model->sun = @$day['sun'];
            $model->header = @$a['home_header'];
            $model->slider1 = @$a['home_slider1'];
            $model->gall1 = @$a['gall_1'];
            $model->gall2 = @$a['gall_2'];
            $model->gall3 = @$a['gall_3'];
            $model->gall4 = @$a['gall_4'];
        } else {
            $model = new Home();
        }
        $model->user_id = Yii::$app->user->id;
        if ($model->load(Yii::$app->request->post())) {
            $hour = array(
                'mon' => $_POST['Home']['mon'],
                'tue' => $_POST['Home']['tue'],
                'wed' => $_POST['Home']['wed'],
                'thu' => $_POST['Home']['thu'],
                'fri' => $_POST['Home']['fri'],
                'sat' => $_POST['Home']['sat'],
                'sun' => $_POST['Home']['sun'],
            );
            $model->working_hour = serialize($hour);
            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'Saved successfully');
                return $this->redirect(['home']);
            } else {
                Yii::$app->session->setFlash('error', "Change few things and try again or contact admin.");
            }
        }

        return $this->render('home', [
            'model' => $model,
        ]);
    }

    public function actionAbout()
    {
        $this->view->title = 'About Page';
        $id = Yii::$app->user->id;
        $query = new Query();
        $query->select('i.home_header, i.about_img')
            ->from('ks_img as i')
            ->join('LEFT JOIN', 'ks_about as a', "a.user_id = $id")
            ->where(['i.user_id' => $id]);
        $command = $query->createCommand();
        $a = $command->queryOne();

        if ($a) {
            $model = About::find()->where(['user_id' => $id])->one();
            if (empty($model)) {
                $model = new About();
            }
            $model->header = @$a['home_header'];
            $model->about_img = @$a['about_img'];
        } else {
            $model = new About();
        }
        $model->user_id = Yii::$app->user->id;
        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'Saved successfully');
                return $this->redirect(['about']);
            } else {
                Yii::$app->session->setFlash('error', "Change few things and try again or contact admin.");
            }
        }
        return $this->render('about', [
            'model' => $model
        ]);
    }

    public function actionGallery()
    {
        $this->view->title = 'Gallery';
        $model = Img::find()->where(['user_id' => Yii::$app->user->id])->one();
        if (empty($model)) {
            $model = new Img();
        }

        return $this->render('gallery', [
            'model' => $model
        ]);
    }

    public function actionContact()
    {
        $this->view->title = 'Contact';
        $id = Yii::$app->user->id;
        $query = new Query();
        $query->select('i.home_header')
            ->from('ks_img as i')
            ->join('LEFT JOIN', 'ks_about as a', "a.user_id = $id")
            ->where(['i.user_id' => $id]);
        $command = $query->createCommand();
        $a = $command->queryOne();
        $model = Vendor::find()->where(['user_id' => $id])->one();
        $model->home_header = @$a['home_header'];
        $model->user_id = $id;
        $model->email = Yii::$app->user->email;
        return $this->render('contact', [
            'model' => $model
        ]);
    }

    public function actionFile()
    {
        if (Yii::$app->request->isAjax) {
            if ($_FILES['img']['name']) {
                $uploadedFile = '';
                if (!empty($_FILES["img"]["type"])) {
                    $valid_extensions = array("jpeg", 'png', 'jpg'); //validation
                    $temporary = explode(".", $_FILES["img"]["name"]);
                    $file_extension = end($temporary);
                    $fileName = $this->generateRandomString() . time() . '.' . $file_extension; //random file name

                    if (($_FILES["img"]["type"] == ("image/jpeg" || "image/png" || "image/JPEG" || "image/PNG")) && in_array($file_extension, $valid_extensions)) //check file extension
                    {
                        $sourcePath = $_FILES['img']['tmp_name'];
                        $path = Yii::getAlias('@webroot') . '/theme/user/' . yii::$app->user->name;
                        if (!file_exists($path)) //folder exsist or not if not then add
                        {
                            mkdir($path, 0755, true);
                        }

                        $targetPath = $path . '/' . $fileName;
                        if (move_uploaded_file($sourcePath, $targetPath)) {
                            $uploadedFile = $fileName;
                            /* 
                            * logo upload
                            */
                            if ($_POST['type'] == '#logo') {
                                $logo = Vendor::find()->where(['user_id' => yii::$app->user->id])->one();
                                @unlink($path . '/' . $logo->image);
                                $logo->image = $uploadedFile;
                                $logo->save(false);

                                $data['msg'] = 'success';
                                $data['status'] = 'Image updated.';
                                $data['image'] = Url::Home(true) . "theme/user/" . Yii::$app->user->name . "/" . $uploadedFile;
                                echo json_encode($data);
                                yii::$app->end();
                            } else {
                                $model = Img::find()->where(['user_id' => yii::$app->user->id])->one();
                                if (empty($model)) {
                                    $model = new img();
                                } else {
                                    ($_POST['type'] == '#home_header') ? @unlink($path . '/' . $model->home_header) : '';
                                    ($_POST['type'] == '#slider1') ? @unlink($path . '/' . $model->home_slider1) : '';
                                    ($_POST['type'] == '#slider2') ? @unlink($path . '/' . $model->home_slider2) : '';
                                    ($_POST['type'] == '#gall1') ? @unlink($path . '/' . $model->gall_1) : '';
                                    ($_POST['type'] == '#gall2') ? @unlink($path . '/' . $model->gall_2) : '';
                                    ($_POST['type'] == '#gall3') ? @unlink($path . '/' . $model->gall_3) : '';
                                    ($_POST['type'] == '#gall4') ? @unlink($path . '/' . $model->gall_4) : '';
                                    ($_POST['type'] == '#gall5') ? @unlink($path . '/' . $model->gall_5) : '';
                                    ($_POST['type'] == '#gall6') ? @unlink($path . '/' . $model->gall_6) : '';
                                    ($_POST['type'] == '#gall7') ? @unlink($path . '/' . $model->gall_7) : '';
                                    ($_POST['type'] == '#gall8') ? @unlink($path . '/' . $model->gall_8) : '';
                                    ($_POST['type'] == '#about_img') ? @unlink($path . '/' . $model->about_img) : '';
                                }
                                $model->user_id  = yii::$app->user->id;

                                ($_POST['type'] == '#home_header') ? $model->home_header = $uploadedFile : '';
                                ($_POST['type'] == '#slider1') ? $model->home_slider1 = $uploadedFile : '';
                                ($_POST['type'] == '#slider2') ? $model->home_slider2 = $uploadedFile : '';
                                ($_POST['type'] == '#gall1') ? $model->gall_1 = $uploadedFile : '';
                                ($_POST['type'] == '#gall2') ? $model->gall_2 = $uploadedFile : '';
                                ($_POST['type'] == '#gall3') ? $model->gall_3 = $uploadedFile : '';
                                ($_POST['type'] == '#gall4') ? $model->gall_4 = $uploadedFile : '';
                                ($_POST['type'] == '#gall5') ? $model->gall_5 = $uploadedFile : '';
                                ($_POST['type'] == '#gall6') ? $model->gall_6 = $uploadedFile : '';
                                ($_POST['type'] == '#gall7') ? $model->gall_7 = $uploadedFile : '';
                                ($_POST['type'] == '#gall8') ? $model->gall_8 = $uploadedFile : '';
                                ($_POST['type'] == '#about_img') ? $model->about_img = $uploadedFile : '';

                                $model->save();
                                $data['msg'] = 'success';
                                $data['status'] = 'Image updated.';
                                $data['image'] = Url::Home(true) . "theme/user/" . Yii::$app->user->name . "/" . $uploadedFile;
                                echo json_encode($data);
                                yii::$app->end();
                            }
                        }
                    } else {
                        $data['msg'] = 'error';
                        $data['status'] = 'File is not image.';
                        echo json_encode($data);
                        yii::$app->end();
                    }
                }
            } else {
                $data['msg'] = 'File not found';
                echo json_encode($data);
                yii::$app->end();
            }
        }
    }

    /*
     * Random strin generator
     * */
    function generateRandomString($length = 6)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString . '-';
    }
}
