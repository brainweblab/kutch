<?php

namespace frontend\controllers;

use frontend\models\About;
use frontend\models\Home;
use frontend\models\Img;
use frontend\models\Vendor;
use yii\db\Query;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;

/**
 * Site controller
 */
class UserController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex($slug)
    {
        $data = $this->findModel($slug);

        $this->layout = 'user-main';
        $this->view->title = ucfirst($data->company_name) . ' - Home';

        $id = $data->user_id;
        $query = new Query();
        $query->select('i.home_header, i.home_slider1, i.home_slider2, i.gall_1, i.gall_2, i.gall_3, i.gall_4, h.short_about, h.working_hour')
            ->from('ks_img as i')
            ->join('LEFT JOIN', 'ks_home as h', "h.user_id = $id")
            ->where(['i.user_id' => $id]);
        $command = $query->createCommand();
        $a = $command->queryOne();

        if ($a) {
            $model = Home::find()->where(['user_id' => $id])->one();
            if (empty($model)) {
                $model = new Home();
            }
            $day = unserialize($a['working_hour']);
            $model->mon = @$day['mon'];
            $model->tue = @$day['tue'];
            $model->wed = @$day['wed'];
            $model->thu = @$day['thu'];
            $model->fri = @$day['fri'];
            $model->sat = @$day['sat'];
            $model->sun = @$day['sun'];
            $model->header = @$a['home_header'];
            $model->slider1 = @$a['home_slider1'];
            $model->gall1 = @$a['gall_1'];
            $model->gall2 = @$a['gall_2'];
            $model->gall3 = @$a['gall_3'];
            $model->gall4 = @$a['gall_4'];
        } else {
            $model = new Home();
        }
        $model->user_id = $id;
        return $this->render('index', [
            'model' => $model,
            'data'=>$data
        ]);
    }

    public function actionAbout($slug)
    {
        $data = $this->findModel($slug);

        $this->layout = 'user-main';
        $this->view->title = ucfirst($data->company_name) . ' - About';

        $id = $data->user_id;
        $query = new Query();
        $query->select('i.home_header, i.about_img')
        ->from('ks_img as i')
        ->join('LEFT JOIN', 'ks_about as a', "a.user_id = $id")
        ->where(['i.user_id' => $id]);
        $command = $query->createCommand();
        $a = $command->queryOne();

        if ($a) {
            $model = About::find()->where(['user_id' => $id])->one();
            if (empty($model)) {
                $model = new About();
            }
            $model->header = @$a['home_header'];
            $model->about_img = @$a['about_img'];
        } else {
            $model = new About();
        }
        $model->user_id = $id;
        return $this->render('about', [
            'model' => $model,
            'data' => $data
        ]);
    }

    public function actionGallery($slug)
    {
        $data = $this->findModel($slug);

        $this->layout = 'user-main';
        $this->view->title = ucfirst($data->company_name) . ' - Gallery';

        $id = $data->user_id;
        $model = Img::find()->where(['user_id' => $id])->one();
        if (empty($model)) {
            $model = new Img();
        }

        return $this->render('gallery', [
            'model' => $model,
            'data' => $data
        ]);
    }

    public function actionContact($slug)
    {
        $data = $this->findModel($slug);

        $this->layout = 'user-main';
        $this->view->title = ucfirst($data->company_name) . ' - Gallery';

        $id = $data->user_id;
        $query = new Query();
        $query->select('i.home_header')
        ->from('ks_img as i')
        ->join('LEFT JOIN', 'ks_about as a', "a.user_id = $id")
        ->where(['i.user_id' => $id]);
        
        $command = $query->createCommand();
        $a = $command->queryOne();
        $model = Vendor::find()->where(['user_id' => $id])->one();
        $model->home_header = @$a['home_header'];
        $model->user_id = $id;
        $model->email = $data->email;

        return $this->render('contact', [
            'model' => $model,
            'data' => $data
        ]);
    }

    private function findModel($slug)
    {
        $vendor  = Vendor::find()->where(['url' => $slug])->one();
        if (empty($vendor)) {
            throw new NotFoundHttpException('Page not found');
        }
        return $vendor;
    }
}
