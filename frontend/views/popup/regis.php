<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
?>
<!--register form -->
<div class="main-register-wrap modal">
    <div class="reg-overlay"></div>
    <div class="main-register-holder tabs-act">
        <div class="main-register fl-wrap  modal_main">
            <div class="main-register_title">Welcome to <span><strong>Town</strong>Hub<strong>.</strong></span></div>
            <div class="close-reg"><i class="fal fa-times"></i></div>
            <ul class="tabs-menu fl-wrap no-list-style">
                <li class="current"><a href="#tab-1"><i class="fal fa-sign-in-alt"></i> Login</a></li>
                <li><a href="#tab-2"><i class="fal fa-user-plus"></i> Register</a></li>
            </ul>
            <!--tabs -->
            <div class="tabs-container">
                <div class="tab">
                    <!--tab -->
                    <div id="tab-1" class="tab-content first-tab">
                        <div class="custom-form">
                            <?php $form = ActiveForm::begin([
                                'id' => 'login-form',
                                'fieldConfig' => [
                                    'template' => "{label}\n{input}\n{error}",
                                    // 'options' => ['class' => 'form-group col-md-3 '],
                                    // 'labelOptions' => ['class' => 'form-control-label'],
                                    'errorOptions' => ['encode' => false, 'class' => 'error-cust'],
                                ],
                                'options' => ['autocomplete' => 'off', 'class' => 'main-register-form'],
                            ]); ?>

                            <?= $form->field($login, 'username')->textInput(['autofocus' => true]) ?>

                            <?= $form->field($login, 'password')->passwordInput() ?>

                            <?= Html::button('Login <i class="fas fa-caret-right"></i>', ['class' => 'btn float-btn color2-bg', 'name' => 'login-button', 'onclick' => "submita(1)"]) ?>
                            <div class="clearfix"></div>

                            <div class="filter-tags">
                                <?= $form->field($login, 'rememberMe')->checkbox() ?>
                            </div>

                            <div class="lost_password">
                                <?= Html::a('Lost Your Password?', ['site/request-password-reset']) ?>.
                            </div>



                            <?php ActiveForm::end(); ?>
                            <!-- <form method="post" name="registerform">
                                <label>Username or Email Address <span>*</span> </label>
                                <input name="email" type="text" onClick="this.select()" value="">
                                <label>Password <span>*</span> </label>
                                <input name="password" type="password" onClick="this.select()" value="">
                                <button type="submit" class="btn float-btn color2-bg"> Log In <i class="fas fa-caret-right"></i></button>
                                <div class="clearfix"></div>
                                <div class="filter-tags">
                                    <input id="check-a3" type="checkbox" name="check">
                                    <label for="check-a3">Remember me</label>
                                </div>
                            </form>
                            <div class="lost_password">
                                <a href="#">Lost Your Password?</a>
                            </div> -->
                        </div>
                    </div>
                    <!--tab end -->
                    <!--tab -->
                    <div class="tab">
                        <div id="tab-2" class="tab-content">
                            <div class="custom-form">
                                <?php $form = ActiveForm::begin([
                                    'fieldConfig' => [
                                        'template' => "{label}\n{input}\n{error}",
                                        // 'options' => ['class' => 'form-group col-md-3 '],
                                        // 'labelOptions' => ['class' => 'form-control-label'],
                                        'errorOptions' => ['encode' => false, 'class' => 'error-cust'],
                                    ],
                                    'options' => ['autocomplete' => 'off', 'class' => 'main-register-form'],
                                ]); ?>

                                <?= $form->field($regis, 'name')->textInput(['autofocus' => true]) ?>

                                <?= $form->field($regis, 'email') ?>

                                <?= $form->field($regis, 'password')->passwordInput() ?>

                                <div class="form-group">
                                    <?= Html::button('Register <i class="fas fa-caret-right"></i>', ['class' => 'btn float-btn color2-bg', 'name' => 'signup-button', 'onclick' => "submita(2)"]) ?>
                                </div>

                                <?php ActiveForm::end(); ?>
                            </div>
                        </div>
                    </div>
                    <!--tab end -->
                </div>
                <!--tabs end -->

                <div class="wave-bg">
                    <div class='wave -one'></div>
                    <div class='wave -two'></div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--register form end -->
<script>
    function submita(id) {
        if (id == 1) {
            form_id = '#login-form';
            input = $(form_id).serialize();
            $.ajax({
                type: "post",
                dataType: "json",
                url: "<?= Yii::$app->urlManager->createAbsoluteUrl(['site/login']) ?>",
                data: input,
                success: function(data) {
                    custerror(form_id, data, input);
                },
            });
        }
        if (id == 2) {
            form_id = '#w0';
            input = $(form_id).serialize();
            $.ajax({
                type: "post",
                dataType: "json",
                url: "<?= Yii::$app->urlManager->createAbsoluteUrl(['site/signup']) ?>",
                data: $(form_id).serialize(),
                success: function(data) {
                    custerror(form_id, data, input);
                },
            });
        }
    }
</script>