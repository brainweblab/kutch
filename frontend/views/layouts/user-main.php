<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;
use yii\helpers\Url;

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">

<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <meta name="robots" content="index, follow" />
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <!--=============== css  ===============-->
    <link type="text/css" rel="stylesheet" href="<?= Url::home(true); ?>theme/css/reset.css">
    <link type="text/css" rel="stylesheet" href="<?= Url::home(true); ?>theme/css/plugins.css">
    <link type="text/css" rel="stylesheet" href="<?= Url::home(true); ?>theme/css/user.css">

    <link type="text/css" rel="stylesheet" href="<?= Url::home(true); ?>theme/css/color.css">
    <!--=============== favicons ===============-->
    <link rel="shortcut icon" href="<?= Url::home(true); ?>theme/images/favicon.ico">
    <script src="<?= Url::home(true); ?>theme/js/jquery.min.js"></script>
    <style>
        .error-cust {
            color: #e83e4e;
            text-align: left;
            font-size: 11px;
            padding-bottom: 5px;
        }

        .has-success>input {
            border-color: #10b759 !important;
        }

        .has-error>input {
            border-color: #e83e4e !important;
        }

        .is-invalid>input {
            border-color: #e83e4e;
        }

        .is-invalid>select {
            border-color: #e83e4e;
        }

        .required label:after {
            content: " *";
        }

        .col-sm-6.field-vendor-url {
            text-align: left !important;
        }

        #vendor-url {
            margin-bottom: 5px;
        }

        .sml {
            font-size: 12px;
            color: #878C9F;
        }

        .absolute-wrap {
            margin-top: 0px;
            background: none;
            border-radius: 0px;
            box-shadow: none;
        }

        .edit-prof_btn_right {
            position: absolute;
            bottom: 6px;
            right: 0px;
            width: 36px;
            height: 36px;
            line-height: 36px;
            border-radius: 100%;
            color: #fff;
            z-index: 4;
            text-align: center;
            box-shadow: 0 9px 26px rgba(58, 87, 135, 0.2);
        }

        .header-logo {
            position: absolute;
            left: -10px;
            height: 100px;
            width: 100px;
            border-radius: 100%;
        }

        .header-logo:before {
            content: '';
            position: absolute;
            left: -5px;
            top: -5px;
            right: -5px;
            bottom: -5px;
            background: #fff;
            z-index: 1;
            border-radius: 100%;
        }

        .header-logo img {
            height: 100px !important;
            width: 100px;
            border-radius: 100%;
            position: relative;
            z-index: 3;
        }
    </style>
    <?php $this->head() ?>
</head>

<body>
    <?php $this->beginBody() ?>

    <!--loader-->
    <div class="loader-wrap">
        <div class="loader-inner">
            <div class="loader-inner-cirle"></div>
        </div>
    </div>
    <!--loader end-->
    <!-- main start  -->
    <div id="main">
        <!-- content-->
        <div class="content">
            <?= $content; ?>
        </div>

        <!-- wrapper end-->
        <!--footer -->
        <footer class="main-footer fl-wrap">
            <!-- footer-header-->
            <div class="footer-header fl-wrap grad ient-dark">
                <div class="container">
                    <div class="row">
                        <div class="col-md-5">
                            <div class="subscribe-header">
                                <h3>Subscribe For a <span>Newsletter</span></h3>
                                <p>Whant to be notified about new locations ? Just sign up.</p>
                            </div>
                        </div>
                        <div class="col-md-7">
                            <div class="subscribe-widget">
                                <div class="subcribe-form">
                                    <form id="subscribe">
                                        <input class="enteremail fl-wrap" name="email" id="subscribe-email" placeholder="Enter Your Email" spellcheck="false" type="text">
                                        <button type="submit" id="subscribe-button" class="subscribe-button"><i class="fal fa-envelope"></i></button>
                                        <label for="subscribe-email" class="subscribe-message"></label>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- footer-header end-->

            <!--sub-footer-->
            <div class="sub-footer  fl-wrap">
                <div class="container">
                    <div class="copyright"> &copy; <?= Html::encode(Yii::$app->name) ?> <?= date('Y') ?>. All rights reserved.</div>
                    <div class="lang-wrap">
                        <div class="show-lang"><span><i class="fal fa-globe-europe"></i><strong>En</strong></span><i class="fa fa-caret-down arrlan"></i></div>

                    </div>
                    <div class="subfooter-nav">
                        <ul class="no-list-style">
                            <li><a href="#">Terms of use</a></li>
                            <li><a href="#">Privacy Policy</a></li>
                            <li><a href="#">Blog</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!--sub-footer end -->
        </footer>
        <!--footer end -->
        <!--map-modal -->
        <div class="map-modal-wrap">
            <div class="map-modal-wrap-overlay"></div>
            <div class="map-modal-item">
                <div class="map-modal-container fl-wrap">
                    <div class="map-modal fl-wrap">
                        <div id="singleMap" data-latitude="40.7" data-longitude="-73.1"></div>
                    </div>
                    <h3><span>Location for : </span><a href="#">Listing Title</a></h3>
                    <div class="map-modal-close"><i class="fal fa-times"></i></div>
                </div>
            </div>
        </div>
        <!--map-modal end -->

        <a class="to-top"><i class="fas fa-caret-up"></i></a>
    </div>

    <?php $this->endBody() ?>
    <!-- Main end -->
    <!--=============== scripts  ===============-->

    <script src="<?= Url::home(true); ?>theme/js/plugins.js"></script>
    <script src="<?= Url::home(true); ?>theme/js/scripts2.js"></script>
    <script src="<?= Url::home(true); ?>theme/js/map-single.js"></script>
    <script src="<?= Url::home(true); ?>theme/js/sweetalert.js"></script>
    <script>
        <?php
        if (Yii::$app->session->hasFlash('success')) {
            $msg = (empty(Yii::$app->session->getFlash("success"))) ? '' : Yii::$app->session->getFlash("success");
            echo '$( document ).ready(function() { swt( "success", "' . $msg . '") });';
        } elseif (Yii::$app->session->hasFlash('error')) {
            echo '$( document ).ready(function() { swt( "error", "' . Yii::$app->session->getFlash('error') . '") });';
        }
        ?>

        function custerror(from, data, input) {
            $.each(data, function(index, element) {
                $(from).yiiActiveForm('updateAttribute', index, element);
            });
        }

        // alert msg
        function swt(type, msg) {
            const Toast = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 5000,
                timerProgressBar: true,
                onOpen: (toast) => {
                    toast.addEventListener('mouseenter', Swal.stopTimer)
                    toast.addEventListener('mouseleave', Swal.resumeTimer)
                }
            })

            Toast.fire({
                icon: type,
                title: msg
            })
        }
    </script>
</body>

</html>
<?php $this->endPage() ?>