<?php

use yii\widgets\Menu;
use yii\helpers\Url;

$logo  = URL::Home(true) . "theme/images/avatar/4.jpg";
if (Yii::$app->user->logo) {
    $logo = URL::Home(true) . "theme/user/" . Yii::$app->user->name . "/" . Yii::$app->user->logo;
}
?>
<link type="text/css" rel="stylesheet" href="<?= Url::home(true); ?>theme/css/dashboard-style.css">

<!--  section  -->
<section class=" parallax-section dashboard-header-sec gradient-bg" data-scrollax-parent="true">
    <div class="container">
        <div class="dashboard-breadcrumbs breadcrumbs"><a href="#">Home</a><a href="#">Dashboard</a><span>Main page</span></div>
        <!--Tariff Plan menu-->
        <div class="tfp-btn"><span>Tariff Plan : </span> <strong>Extended</strong></div>
        <div class="tfp-det">
            <p>You Are on <a href="#">Extended</a> . Use link bellow to view details or upgrade. </p>
            <a href="#" class="tfp-det-btn color2-bg">Details</a>
        </div>
        <!--Tariff Plan menu end-->
        <div class="dashboard-header_conatiner fl-wrap dashboard-header_title">
            <h1>Welcome : <span><?= ucfirst(Yii::$app->user->name) ?></span></h1>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="dashboard-header fl-wrap">
        <div class="container">
            <div class="dashboard-header_conatiner fl-wrap">
                <div class="dashboard-header-avatar">

                    <img src="<?= $logo ?>" alt="">
                    <a href="javascript:void(0)" class="color-bg edit-prof_btn" data-tag="logo"><i class="fal fa-edit"></i></a>
                    <input id="logo" type="file" name="logo" style="display: none;" onchange="test()" />
                </div>
                <div class="dashboard-header-stats-wrap">
                    <div class="dashboard-header-stats">
                        <div class="swiper-container">
                            <div class="swiper-wrapper">
                                <!--  dashboard-header-stats-item -->
                                <div class="swiper-slide">
                                    <div class="dashboard-header-stats-item">
                                        <i class="fal fa-map-marked"></i>
                                        Active Listings
                                        <span>21</span>
                                    </div>
                                </div>
                                <!--  dashboard-header-stats-item end -->
                                <!--  dashboard-header-stats-item -->
                                <div class="swiper-slide">
                                    <div class="dashboard-header-stats-item">
                                        <i class="fal fa-chart-bar"></i>
                                        Listing Views
                                        <span>1054</span>
                                    </div>
                                </div>
                                <!--  dashboard-header-stats-item end -->
                                <!--  dashboard-header-stats-item -->
                                <div class="swiper-slide">
                                    <div class="dashboard-header-stats-item">
                                        <i class="fal fa-comments-alt"></i>
                                        Total Reviews
                                        <span>79</span>
                                    </div>
                                </div>
                                <!--  dashboard-header-stats-item end -->
                                <!--  dashboard-header-stats-item -->
                                <div class="swiper-slide">
                                    <div class="dashboard-header-stats-item">
                                        <i class="fal fa-heart"></i>
                                        Times Bookmarked
                                        <span>654</span>
                                    </div>
                                </div>
                                <!--  dashboard-header-stats-item end -->
                            </div>
                        </div>
                    </div>
                    <!--  dashboard-header-stats  end -->
                    <div class="dhs-controls">
                        <div class="dhs dhs-prev"><i class="fal fa-angle-left"></i></div>
                        <div class="dhs dhs-next"><i class="fal fa-angle-right"></i></div>
                    </div>
                </div>
                <!--  dashboard-header-stats-wrap end -->
                <a class="add_new-dashboard">Add Listing <i class="fal fa-eye"></i></a>
            </div>
        </div>
    </div>
    <div class="gradient-bg-figure" style="right:-30px;top:10px;"></div>
    <div class="gradient-bg-figure" style="left:-20px;bottom:30px;"></div>
    <div class="circle-wrap" style="left:120px;bottom:120px;" data-scrollax="properties: { translateY: '-200px' }">
        <div class="circle_bg-bal circle_bg-bal_small"></div>
    </div>
    <div class="circle-wrap" style="right:420px;bottom:-70px;" data-scrollax="properties: { translateY: '150px' }">
        <div class="circle_bg-bal circle_bg-bal_big"></div>
    </div>
    <div class="circle-wrap" style="left:420px;top:-70px;" data-scrollax="properties: { translateY: '100px' }">
        <div class="circle_bg-bal circle_bg-bal_big"></div>
    </div>
    <div class="circle-wrap" style="left:40%;bottom:-70px;">
        <div class="circle_bg-bal circle_bg-bal_middle"></div>
    </div>
    <div class="circle-wrap" style="right:40%;top:-10px;">
        <div class="circle_bg-bal circle_bg-bal_versmall" data-scrollax="properties: { translateY: '-350px' }"></div>
    </div>
    <div class="circle-wrap" style="right:55%;top:90px;">
        <div class="circle_bg-bal circle_bg-bal_versmall" data-scrollax="properties: { translateY: '-350px' }"></div>
    </div>
</section>

<!--  section  end-->
<section class="gray-bg main-dashboard-sec" id="sec1">
    <div class="container">
        <!--  dashboard-menu-->
        <div class="col-md-3">
            <div class="mob-nav-content-btn color2-bg init-dsmen fl-wrap"><i class="fal fa-bars"></i> Dashboard menu</div>
            <div class="clearfix"></div>
            <div class="fixed-bar fl-wrap" id="dash_menu">
                <div class="user-profile-menu-wrap fl-wrap block_box">
                    <!-- user-profile-menu-->
                    <div class="user-profile-menu">
                        <h3>Main</h3>
                        <?php
                        echo Menu::widget([
                            'items' => [
                                ['label' => '<i class="fal fa-chart-line"></i>Dashboard', 'url' => ['//dashboard/index']],
                                ['label' => '<i class="fal fa-home"></i>Home Page', 'url' => ['//dashboard/home']],
                                ['label' => '<i class="fal fa-file-alt"></i>About Page', 'url' => ['//dashboard/about']],
                                ['label' => '<i class="fal fa-images"></i>Gallery', 'url' => ['//dashboard/gallery']],
                                ['label' => '<i class="fal fa-id-card"></i>Contact', 'url' => ['//dashboard/contact']],
                            ],
                            'activateParents' => true,
                            'encodeLabels' => false,
                            'activeCssClass' => 'user-profile-act',
                            'options' => [
                                'class' => 'no-list-style',
                            ],
                        ]);
                        ?>
                    </div>
                    <a class="logout_btn color2-bg" href="<?= Url::to(['site/logout']) ?>">Log Out <i class="fas fa-sign-out"></i></a>
                </div>
            </div>
            <a class="back-tofilters color2-bg custom-scroll-link fl-wrap" href="#dash_menu">Back to Menu<i class="fas fa-caret-up"></i></a>
        </div>
        <!-- dashboard-menu  end-->
        <!-- dashboard content-->
        <div class="col-md-9">
            <?= $content ?>
        </div>
</section>
<!--  section  end-->
<div class="limit-box fl-wrap"></div>

<script src="<?= Url::home(true); ?>theme/js/charts.js"></script>
<script src="<?= Url::home(true); ?>theme/js/dashboard.js"></script>
<script>
    var id;
    $('a').on('click', function() {
        id = '#' + $(this).attr('data-tag');
        $(id).trigger('click');
    });

    function test() {
        var file = $(id).prop('files')[0];
        var imagefile = file.type;
        var match = ["image/jpeg", "image/png", "image/JPEG", "image/PNG"];
        if (!((imagefile == match[0]) || (imagefile == match[1]) || (imagefile == match[2]))) {
            swt("error", 'Uploaded file is not image.');
            $(id).val('');
            return false;
        } else {
            submit(id);
        }
    }

    //header image
    function homeHeader(obj) {
        $('#temp_i').hide();
        $('#final_i').show();
        $('.par-elem').attr('data-bg', obj.image);
        $('.parth').css({
            'background-image': 'url("' + obj.image + '")'
        });
    }

    //slider1 image
    function slider(obj) {
        $('.respimg').attr('src', obj.image);
    }

    //gall image
    function gall(obj, id) {
        $(id).closest('div').find('img').attr('src', obj.image);
        $(id).closest('div').find('a').attr('href', obj.image);
    }

    function about_img(obj, id) {
        $(id).closest('div').find('img').attr('src', obj.image);
    }

    function logo(obj, id) {
        $(id).closest('div').find('img').attr('src', obj.image);
    }

    //image upload
    function submit(id) {
        var file_data = $(id).prop('files')[0];
        var formData = new FormData();
        formData.append('img', file_data);
        formData.append('type', id);
        $.ajax({
            type: 'POST',
            url: '<?= \Yii::$app->urlManager->createAbsoluteUrl(["/dashboard/file"]) ?>',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function(data) {
                var obj = jQuery.parseJSON(data);
                if (obj.msg == 'success') {
                    var msg = obj.msg.toUpperCase();
                    (id == '#home_header') ? homeHeader(obj): '';
                    (id == '#slider1') ? slider(obj): '';
                    (id == '#gall1') ? gall(obj, id): '';
                    (id == '#gall2') ? gall(obj, id): '';
                    (id == '#gall3') ? gall(obj, id): '';
                    (id == '#gall4') ? gall(obj, id): '';
                    (id == '#gall5') ? gall(obj, id): '';
                    (id == '#gall6') ? gall(obj, id): '';
                    (id == '#gall7') ? gall(obj, id): '';
                    (id == '#gall8') ? gall(obj, id): '';
                    (id == '#logo') ? logo(obj, id): '';
                    (id == '#about_img') ? about_img(obj, id): '';
                    $(id).val('');
                    swt(obj.msg, obj.status);
                } else {
                    $(id).val('');
                    swt(obj.msg, obj.status);
                }
            }
        });
    };

    CKEDITOR.instances.editor.on('change', function() {
        textSave('home');
    });

    function textSave(page) {
        $('.to-top-custom').show();
    }

    //texarea
    $().on
</script>