<?php

use yii\helpers\Url;

$style1 = '';
$style2 = '';
$url = '';
if ($model->home_header) {
    $style2 = 'display:none';
    $url = Url::Home(true) . "theme/user/" . Yii::$app->user->name . "/" . $model->home_header;
} else {
    $style1 = 'display:none';
}

$address = wordwrap(Yii::$app->user->vendor()->address, 40, " <br>", TRUE);
$phone = (Yii::$app->user->vendor()->phone_alt) ? Yii::$app->user->vendor()->phone_alt : Yii::$app->user->vendor()->phone;
$email = ucfirst((Yii::$app->user->vendor()->email_alt) ? Yii::$app->user->vendor()->email_alt : Yii::$app->user->email);
$website = Yii::$app->user->vendor()->website;
$fb = (Yii::$app->user->vendor()->facebook) ? Yii::$app->user->vendor()->facebook : "#";
$insta = (Yii::$app->user->vendor()->instagram) ?  Yii::$app->user->vendor()->instagram : "#";
$twit = (Yii::$app->user->vendor()->twitter) ? Yii::$app->user->vendor()->twitter : "#";

$logo  = URL::Home(true) . "theme/images/avatar/4.jpg";
if (Yii::$app->user->logo) {
    $logo = URL::Home(true) . "theme/user/" . Yii::$app->user->name . "/" . Yii::$app->user->logo;
}
?>
<div class="content">
    <section class="listing-hero-section hidden-section" data-scrollax-parent="true" id="sec1">

        <div class="bg-parallax-wrap" style="<?= $style1 ?>" id='final_i'>
            <div class="bg par-elem parth" data-bg="<?= $url ?>" data-scrollax="properties: { translateY: '30%' }"></div>
            <div class="overlay"></div>

        </div>
        <div class="bg-parallax-wrap gradient-bg" style="<?= $style2 ?>" id='temp_i'>
            <div class="gradient-bg-figure" style="right:-30px;top:10px;"></div>
            <div class="gradient-bg-figure" style="left:-20px;bottom:30px;"></div>
            <div class="circle-wrap" style="left:120px;bottom:120px;" data-scrollax="properties: { translateY: '-200px' }">
                <div class="circle_bg-bal circle_bg-bal_small"></div>
            </div>
            <div class="circle-wrap" style="right:420px;bottom:-70px;" data-scrollax="properties: { translateY: '150px' }">
                <div class="circle_bg-bal circle_bg-bal_big"></div>
            </div>
            <div class="circle-wrap" style="left:420px;top:-70px;" data-scrollax="properties: { translateY: '100px' }">
                <div class="circle_bg-bal circle_bg-bal_big"></div>
            </div>
            <div class="circle-wrap" style="left:40%;bottom:-70px;">
                <div class="circle_bg-bal circle_bg-bal_middle"></div>
            </div>
            <div class="circle-wrap" style="right:40%;top:-10px;">
                <div class="circle_bg-bal circle_bg-bal_versmall" data-scrollax="properties: { translateY: '-350px' }"></div>
            </div>
            <div class="circle-wrap" style="right:55%;top:90px;">
                <div class="circle_bg-bal circle_bg-bal_versmall" data-scrollax="properties: { translateY: '-350px' }"></div>
            </div>
        </div>

        <div class="container">
            <div class="list-single-header-item  fl-wrap">
                <div class="row">
                    <div class="col-md-9">
                        <div class="col-md-2">
                            <div class="header-logo">
                                <img src="<?= $logo ?>" alt="">
                            </div>
                        </div>
                        <div class="col-md-10">
                            <h1><?= ucfirst(Yii::$app->user->name) ?><span class="verified-badge"><i class="fal fa-check"></i></span></h1>
                            <div class="geodir-category-location fl-wrap">
                                <a href="#"><i class="fas fa-map-marker-alt"></i> <?= $address ?></a>
                                <a href="tel:<?= $phone ?>"> <i class="fal fa-phone"></i><?= $phone ?></a>
                                <a href="mailto:<?= $email ?>"><i class="fal fa-envelope"></i> <?= $email ?></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <a class="fl-wrap list-single-header-column custom-scroll-link " href="#sec5">
                            <div class="listing-rating-count-wrap single-list-count">
                                <div class="review-score">4.1</div>
                                <div class="listing-rating card-popup-rainingvis" data-starrating2="4"></div>
                                <br>
                                <div class="reviews-count">2 reviews</div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            <div class="list-single-header_bottom fl-wrap">
                <a class="listing-item-category-wrap" href="#">
                    <div class="listing-item-category  red-bg"><i class="fal fa-cheeseburger"></i></div>
                    <span>Restaurants</span>
                </a>
                <div class="list-single-author"> <a href="author-single.html"><span class="author_avatar"> <img alt='' src='images/avatar/5.jpg'> </span>By Alisa Noory</a></div>
                <div class="geodir_status_date gsd_open"><i class="fal fa-lock-open"></i>Open Now</div>
                <div class="list-single-stats">
                    <ul class="no-list-style">
                        <li><span class="viewed-counter"><i class="fas fa-eye"></i> Viewed - 156 </span></li>
                        <li><span class="bookmark-counter"><i class="fas fa-heart"></i> Bookmark - 24 </span></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <!-- scroll-nav-wrapper-->
    <div class="scroll-nav-wrapper fl-wrap">
        <div class="container">
            <nav class="scroll-nav ">
                <ul class="no-list-style">
                    <li><a href="<?= Url::to(['//' . Yii::$app->user->custurl . '/index'], true) ?>"><i class="fal fa-images"></i> Home</a></li>
                    <li><a href="<?= Url::to(['//' . Yii::$app->user->custurl . '/about'], true) ?>"><i class="fal fa-info"></i>About</a></li>
                    <li><a href="<?= Url::to(['//' . Yii::$app->user->custurl . '/gallery'], true) ?>"><i class="fal fa-image"></i>Gallery</a></li>
                    <li><a class="act-scrlink" href="<?= Url::to(['//' . Yii::$app->user->custurl . '/contact'], true) ?>"><i class="fal fa-id-card"></i>Contact</a></li>
                </ul>
            </nav>
            <div class="scroll-nav-wrapper-opt">
                <a href="#" class="scroll-nav-wrapper-opt-btn"> <i class="fas fa-heart"></i> Save </a>
                <a href="#" class="scroll-nav-wrapper-opt-btn showshare"> <i class="fas fa-share"></i> Share </a>
                <div class="share-holder hid-share">
                    <div class="share-container  isShare"></div>
                </div>
                <div class="show-more-snopt"><i class="fal fa-ellipsis-h"></i></div>
                <div class="show-more-snopt-tooltip">
                    <a href="#"> <i class="fas fa-comment-alt"></i> Write a review</a>
                    <a href="#"> <i class="fas fa-flag-alt"></i> Report </a>
                </div>
            </div>
        </div>
    </div>

    <!--  section  -->
    <section id="sec1" data-scrollax-parent="true">
        <div class="container">
            <!--about-wrap -->
            <div class="about-wrap">
                <div class="row">
                    <div class="col-md-4">
                        <div class="ab_text-title fl-wrap">
                            <h3>Get in Touch</h3>
                            <span class="section-separator fl-sec-sep"></span>
                        </div>
                        <!--box-widget-item -->
                        <div class="box-widget-item fl-wrap block_box">
                            <div class="box-widget">
                                <div class="box-widget-content bwc-nopad">
                                    <div class="list-author-widget-contacts list-item-widget-contacts bwc-padside">
                                        <ul class="no-list-style">
                                            <li><span><i class="fal fa-map-marker"></i> Adress :</span> <a href="#singleMap" class="custom-scroll-link"><?= $address ?></a></li>
                                            <li><span><i class="fal fa-phone"></i> Phone :</span> <a href="<tel:?= $phone ?>"><?= $phone ?></a></li>
                                            <li><span><i class="fal fa-envelope"></i> Mail :</span> <a href="mailto:<?= $email ?>"><?= $email ?></a></li>
                                        </ul>
                                    </div>
                                    <div class="list-widget-social bottom-bcw-box  fl-wrap">
                                        <ul class="no-list-style">
                                            <li><a href="<?=$fb?>" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                                            <li><a href="<?=$insta?>" target="_blank"><i class="fab fa-twitter"></i></a></li>
                                            <li><a href="<?=$twit?>" target="_blank"><i class="fab fa-instagram"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--box-widget-item end -->
                        <!--box-widget-item -->
                        <div class="box-widget-item fl-wrap" style="margin-top:20px;">
                            <div class="banner-wdget fl-wrap">
                                <div class="overlay op4"></div>
                                <div class="bg" data-bg="images/bg/18.jpg"></div>
                                <div class="banner-wdget-content fl-wrap">
                                    <h4>Participate in our loyalty program. Refer a friend and get a discount.</h4>
                                    <a href="#" class="color-bg">Read more</a>
                                </div>
                            </div>
                        </div>
                        <!--box-widget-item end -->
                    </div>
                    <div class="col-md-8">
                        <div class="ab_text">
                            <div class="ab_text-title fl-wrap">
                                <h3>Contact Us</h3>
                                <span class="section-separator fl-sec-sep"></span>
                            </div>

                            <div id="contact-form">
                                <div id="message"></div>
                                <form class="custom-form" action="http://townhub.kwst.net/php/contact.php" name="contactform" id="contactform">
                                    <fieldset>
                                        <label><i class="fal fa-user"></i></label>
                                        <input type="text" name="name" id="name" placeholder="Your Name *" value="" />
                                        <div class="clearfix"></div>
                                        <label><i class="fal fa-envelope"></i> </label>
                                        <input type="text" name="email" id="email" placeholder="Email Address*" value="" />
                                        <textarea name="comments" id="comments" cols="40" rows="3" placeholder="Your Message:"></textarea>
                                    </fieldset>
                                    <button class="btn float-btn color2-bg" id="submit">Send Message<i class="fal fa-paper-plane"></i></button>
                                </form>
                            </div>
                            <!-- contact form  end-->
                        </div>
                    </div>
                </div>
            </div>
            <!-- about-wrap end  -->
        </div>
    </section>
</div>