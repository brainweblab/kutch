<?php

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\helpers\Url;


$style1 = '';
$style2 = '';
$url = '';
if ($model->header) {
    $style2 = 'display:none';
    $url = URL::Home(true) . "theme/user/" . Yii::$app->user->name . "/" . $model->header;
} else {
    $style1 = 'display:none';
}

$ab = URL::Home(true) . "theme/images/all/55.jpg";
if ($model->about_img) {
    $ab = URL::Home(true) . "theme/user/" . Yii::$app->user->name . "/" . $model->about_img;
}

$address = wordwrap(Yii::$app->user->vendor()->address, 40, " <br>", TRUE);
$phone = (Yii::$app->user->vendor()->phone_alt) ? Yii::$app->user->vendor()->phone_alt : Yii::$app->user->vendor()->phone;
$email = ucfirst((Yii::$app->user->vendor()->email_alt) ? Yii::$app->user->vendor()->email_alt : Yii::$app->user->email);
$website = Yii::$app->user->vendor()->website;

$logo  = URL::Home(true) . "theme/images/avatar/4.jpg";
if (Yii::$app->user->logo) {
    $logo = URL::Home(true) . "theme/user/" . Yii::$app->user->name . "/" . Yii::$app->user->logo;
}
?>
<div class="content">
    <section class="listing-hero-section hidden-section" data-scrollax-parent="true" id="sec1">

        <div class="bg-parallax-wrap" style="<?= $style1 ?>" id='final_i'>
            <div class="bg par-elem parth" data-bg="<?= $url ?>" data-scrollax="properties: { translateY: '30%' }"></div>
            <div class="overlay"></div>

        </div>
        <div class="bg-parallax-wrap gradient-bg" style="<?= $style2 ?>" id='temp_i'>
            <div class="gradient-bg-figure" style="right:-30px;top:10px;"></div>
            <div class="gradient-bg-figure" style="left:-20px;bottom:30px;"></div>
            <div class="circle-wrap" style="left:120px;bottom:120px;" data-scrollax="properties: { translateY: '-200px' }">
                <div class="circle_bg-bal circle_bg-bal_small"></div>
            </div>
            <div class="circle-wrap" style="right:420px;bottom:-70px;" data-scrollax="properties: { translateY: '150px' }">
                <div class="circle_bg-bal circle_bg-bal_big"></div>
            </div>
            <div class="circle-wrap" style="left:420px;top:-70px;" data-scrollax="properties: { translateY: '100px' }">
                <div class="circle_bg-bal circle_bg-bal_big"></div>
            </div>
            <div class="circle-wrap" style="left:40%;bottom:-70px;">
                <div class="circle_bg-bal circle_bg-bal_middle"></div>
            </div>
            <div class="circle-wrap" style="right:40%;top:-10px;">
                <div class="circle_bg-bal circle_bg-bal_versmall" data-scrollax="properties: { translateY: '-350px' }"></div>
            </div>
            <div class="circle-wrap" style="right:55%;top:90px;">
                <div class="circle_bg-bal circle_bg-bal_versmall" data-scrollax="properties: { translateY: '-350px' }"></div>
            </div>
        </div>

        <div class="container">
            <div class="list-single-header-item  fl-wrap">
                <div class="row">
                    <div class="col-md-9">
                        <div class="col-md-2">
                            <div class="header-logo">
                                <img src="<?= $logo ?>" alt="">
                            </div>
                        </div>
                        <div class="col-md-10">
                            <h1><?= ucfirst(Yii::$app->user->name) ?><span class="verified-badge"><i class="fal fa-check"></i></span></h1>
                            <div class="geodir-category-location fl-wrap">
                                <a href="#"><i class="fas fa-map-marker-alt"></i> <?= $address ?></a>
                                <a href="tel:<?= $phone ?>"> <i class="fal fa-phone"></i><?= $phone ?></a>
                                <a href="mailto:<?= $email ?>"><i class="fal fa-envelope"></i> <?= $email ?></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <a class="fl-wrap list-single-header-column custom-scroll-link " href="#sec5">
                            <div class="listing-rating-count-wrap single-list-count">
                                <div class="review-score">4.1</div>
                                <div class="listing-rating card-popup-rainingvis" data-starrating2="4"></div>
                                <br>
                                <div class="reviews-count">2 reviews</div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            <div class="list-single-header_bottom fl-wrap">
                <a class="listing-item-category-wrap" href="#">
                    <div class="listing-item-category  red-bg"><i class="fal fa-cheeseburger"></i></div>
                    <span>Restaurants</span>
                </a>
                <div class="list-single-author"> <a href="author-single.html"><span class="author_avatar"> <img alt='' src='images/avatar/5.jpg'> </span>By Alisa Noory</a></div>
                <div class="geodir_status_date gsd_open"><i class="fal fa-lock-open"></i>Open Now</div>
                <div class="list-single-stats">
                    <ul class="no-list-style">
                        <li><span class="viewed-counter"><i class="fas fa-eye"></i> Viewed - 156 </span></li>
                        <li><span class="bookmark-counter"><i class="fas fa-heart"></i> Bookmark - 24 </span></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <!-- scroll-nav-wrapper-->
    <div class="scroll-nav-wrapper fl-wrap">
        <div class="container">
            <nav class="scroll-nav">
                <ul class="no-list-style">
                    <li><a href="<?= Url::to(['//' . Yii::$app->user->custurl . '/index'], true) ?>"><i class="fal fa-images"></i> Home</a></li>
                    <li><a class="act-scrlink" href="<?= Url::to(['//' . Yii::$app->user->custurl . '/about'], true) ?>"><i class="fal fa-info"></i>About</a></li>
                    <li><a href="<?= Url::to(['//' . Yii::$app->user->custurl . '/gallery'], true) ?>"><i class="fal fa-image"></i>Gallery</a></li>
                    <li><a href="<?= Url::to(['//' . Yii::$app->user->custurl . '/contact'], true) ?>"><i class="fal fa-id-card"></i>Contact</a></li>
                </ul>
            </nav>
            <div class="scroll-nav-wrapper-opt">
                <a href="#" class="scroll-nav-wrapper-opt-btn"> <i class="fas fa-heart"></i> Save </a>
                <a href="#" class="scroll-nav-wrapper-opt-btn showshare"> <i class="fas fa-share"></i> Share </a>
                <div class="share-holder hid-share">
                    <div class="share-container  isShare"></div>
                </div>
                <div class="show-more-snopt"><i class="fal fa-ellipsis-h"></i></div>
                <div class="show-more-snopt-tooltip">
                    <a href="#"> <i class="fas fa-comment-alt"></i> Write a review</a>
                    <a href="#"> <i class="fas fa-flag-alt"></i> Report </a>
                </div>
            </div>
        </div>
    </div>

    <!-- scroll-nav-wrapper end-->
    <section id="sec1" data-scrollax-parent="true">
        <div class="container">
            <div class="section-title">
                <h2> How We Work</h2>
                <div class="section-subtitle">who we are</div>
                <span class="section-separator"></span>
                <p>Explore some of the best tips from around the city from our partners and friends.</p>
            </div>
            <!--about-wrap -->
            <div class="about-wrap">
                <div class="row">
                    <div class="col-md-6">
                        <div class="list-single-main-media fl-wrap" style="box-shadow: 0 9px 26px rgba(58, 87, 135, 0.2);">
                            <img src="<?= $ab ?>" class="respimg" alt="">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="ab_text">
                            <div class="ab_text-title fl-wrap">
                                <h3>Our Awesome Team <span>Story</span></h3>
                                <span class="section-separator fl-sec-sep"></span>
                            </div>
                            <div><?= $model->about ?></div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- about-wrap end  -->
            <div class="section-separator"></div>
            <div class="absolute-wrap">
                <!-- features-box-container -->
                <div class="features-box-container fl-wrap">
                    <div class="row">
                        <!--features-box -->
                        <div class="col-md-4">
                            <div class="features-box">
                                <div class="time-line-icon">
                                    <i class="fal fa-headset"></i>
                                </div>
                                <h3>Our Vision</h3>
                                <?= $model->vision ?>
                            </div>
                        </div>
                        <!-- features-box end  -->
                        <!--features-box -->
                        <div class="col-md-4">
                            <div class="features-box">
                                <div class="time-line-icon">
                                    <i class="fal fa-users-cog"></i>
                                </div>
                                <h3>Our Mission</h3>
                                <?= $model->mission ?>
                            </div>
                        </div>
                        <!-- features-box end  -->
                        <!--features-box -->
                        <div class="col-md-4">
                            <div class="features-box ">
                                <div class="time-line-icon">
                                    <i class="fal fa-mobile"></i>
                                </div>
                                <h3>Our Goal</h3>
                                <?= $model->goal ?>
                            </div>
                        </div>
                        <!-- features-box end  -->
                    </div>
                </div>
                <!-- features-box-container end  -->
            </div>
        </div>
    </section>
    <!--section end-->
</div>