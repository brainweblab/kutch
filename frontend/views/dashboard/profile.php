<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model frontend\models\Vendor */
$form = ActiveForm::begin([
    'fieldConfig' => [
        'options' => ['class' => 'col-sm-6'],
        'errorOptions' => ['encode' => false, 'class' => 'error-cust'],
    ],
    'options' => ['autocomplete' => 'off'],
]); ?>
<div class="dashboard-title fl-wrap">
    <h3>Your Profile</h3>
</div>
<!-- profile-edit-container-->
<div class="profile-edit-container fl-wrap block_box">
    <div class="custom-form">
        <div class="row">
            <?php
            $a = "<small class='sml'>E.g. " . Url::to(['//abc-shop'], true) . "</small>";
            echo $form->field($model, 'url', ['template' => "{label}\n{input}\n$a\n{error}"])->textInput(['maxlength' => true, "placeholder" => "abc-shop"])->label('Custom Url<i class="far fa-globe"></i>') ?>

            <?= $form->field($model, 'website')->textInput(['maxlength' => true])->label('Website<i class="far fa-globe"></i>') ?>
        </div>
        <div class="row">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true])->label('Owner Name<i class="fal fa-user"></i>') ?>

            <?= $form->field($model, 'company_name')->textInput(['maxlength' => true])->label('Comapny Name<i class="fal fa-user"></i>') ?>
        </div>
        <div class="row">
            <?= $form->field($model, 'phone')->textInput(['maxlength' => true])->label('Phone No.<i class="far fa-phone"></i>') ?>

            <?= $form->field($model, 'phone_alt')->textInput(['maxlength' => true])->label('Bussines Phone No.<i class="far fa-phone"></i>') ?>
        </div>
        <div class="row">
            <?= $form->field($model, 'email')->textInput(['maxlength' => true])->label('Email<i class="far fa-envelope"></i>') ?>

            <?= $form->field($model, 'email_alt')->textInput(['maxlength' => true])->label('Bussiness Email<i class="far fa-envelope"></i>') ?>
        </div>
        <div class="row">
            <?= $form->field($model, 'address')->textarea(['maxlength' => 255])->label('Address<i class="far fa-address-book"></i>') ?>
        </div>
    </div>


</div>
<!-- profile-edit-container end-->
<div class="dashboard-title dt-inbox fl-wrap">
    <h3>Your Socials</h3>
</div>
<!-- profile-edit-container-->
<div class="profile-edit-container fl-wrap block_box">
    <div class="custom-form">
        <div class="row">
            <?= $form->field($model, 'facebook')->textInput(['maxlength' => true])->label('Facebook<i class="fab fa-facebook"></i>') ?>

            <?= $form->field($model, 'instagram')->textInput(['maxlength' => true])->label('Instagram<i class="fab fa-instagram"></i>') ?>
        </div>
        <div class="row">
            <?= $form->field($model, 'twitter')->textInput(['maxlength' => true])->label('Twitter<i class="fab fa-twitter"></i>') ?>
        </div>
        <?= Html::submitButton('Save <i class="fal fa-save"></i>', ['class' => 'btn color2-bg float-btn']) ?>
    </div>
</div>
<?php ActiveForm::end(); ?>