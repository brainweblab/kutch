<?php

use yii\helpers\Url;

$gall_1 = URL::Home(true) . "theme/images/all/21.jpg";
$gall_2 = URL::Home(true) . "theme/images/all/21.jpg";
$gall_3 = URL::Home(true) . "theme/images/all/21.jpg";
$gall_4 = URL::Home(true) . "theme/images/all/21.jpg";
$gall_5 = URL::Home(true) . "theme/images/all/21.jpg";
$gall_6 = URL::Home(true) . "theme/images/all/21.jpg";
$gall_7 = URL::Home(true) . "theme/images/all/21.jpg";
$gall_8 = URL::Home(true) . "theme/images/all/21.jpg";

$style1 = '';
$style2 = '';
$url = '';
if ($model->home_header) {
    $style2 = 'display:none';
    $url = Url::Home(true) . "theme/user/" . Yii::$app->user->name . "/" . $model->home_header;
} else {
    $style1 = 'display:none';
}

if ($model->gall_1) {
    $gall_1 = URL::Home(true) . "theme/user/" . Yii::$app->user->name . "/" . $model->gall_1;
}

if ($model->gall_2) {
    $gall_2 = URL::Home(true) . "theme/user/" . Yii::$app->user->name . "/" . $model->gall_2;
}

if ($model->gall_3) {
    $gall_3 = URL::Home(true) . "theme/user/" . Yii::$app->user->name . "/" . $model->gall_3;
}

if ($model->gall_4) {
    $gall_4 = URL::Home(true) . "theme/user/" . Yii::$app->user->name . "/" . $model->gall_4;
}
if ($model->gall_5) {
    $gall_5 = URL::Home(true) . "theme/user/" . Yii::$app->user->name . "/" . $model->gall_5;
}

if ($model->gall_6) {
    $gall_6 = URL::Home(true) . "theme/user/" . Yii::$app->user->name . "/" . $model->gall_6;
}

if ($model->gall_7) {
    $gall_7 = URL::Home(true) . "theme/user/" . Yii::$app->user->name . "/" . $model->gall_7;
}

if ($model->gall_8) {
    $gall_8 = URL::Home(true) . "theme/user/" . Yii::$app->user->name . "/" . $model->gall_8;
}

$address = wordwrap(Yii::$app->user->vendor()->address, 40, " <br>", TRUE);
$phone = (Yii::$app->user->vendor()->phone_alt) ? Yii::$app->user->vendor()->phone_alt : Yii::$app->user->vendor()->phone;
$email = ucfirst((Yii::$app->user->vendor()->email_alt) ? Yii::$app->user->vendor()->email_alt : Yii::$app->user->email);
$website = Yii::$app->user->vendor()->website;

$logo  = URL::Home(true) . "theme/images/avatar/4.jpg";
if (Yii::$app->user->logo) {
    $logo = URL::Home(true) . "theme/user/" . Yii::$app->user->name . "/" . Yii::$app->user->logo;
}
?>

<section class="listing-hero-section hidden-section" data-scrollax-parent="true" id="sec1">
    <div class="bg-parallax-wrap" style="<?= $style1 ?>" id='final_i'>
        <div class="bg par-elem parth" data-bg="<?= $url ?>" data-scrollax="properties: { translateY: '30%' }"></div>
        <div class="overlay"></div>

    </div>
    <div class="bg-parallax-wrap gradient-bg" style="<?= $style2 ?>" id='temp_i'>
        <div class="gradient-bg-figure" style="right:-30px;top:10px;"></div>
        <div class="gradient-bg-figure" style="left:-20px;bottom:30px;"></div>
        <div class="circle-wrap" style="left:120px;bottom:120px;" data-scrollax="properties: { translateY: '-200px' }">
            <div class="circle_bg-bal circle_bg-bal_small"></div>
        </div>
        <div class="circle-wrap" style="right:420px;bottom:-70px;" data-scrollax="properties: { translateY: '150px' }">
            <div class="circle_bg-bal circle_bg-bal_big"></div>
        </div>
        <div class="circle-wrap" style="left:420px;top:-70px;" data-scrollax="properties: { translateY: '100px' }">
            <div class="circle_bg-bal circle_bg-bal_big"></div>
        </div>
        <div class="circle-wrap" style="left:40%;bottom:-70px;">
            <div class="circle_bg-bal circle_bg-bal_middle"></div>
        </div>
        <div class="circle-wrap" style="right:40%;top:-10px;">
            <div class="circle_bg-bal circle_bg-bal_versmall" data-scrollax="properties: { translateY: '-350px' }"></div>
        </div>
        <div class="circle-wrap" style="right:55%;top:90px;">
            <div class="circle_bg-bal circle_bg-bal_versmall" data-scrollax="properties: { translateY: '-350px' }"></div>
        </div>
    </div>

    <div class="container">
        <div class="list-single-header-item  fl-wrap">
            <div class="row">
                <div class="col-md-9">
                    <div class="col-md-2">
                        <div class="header-logo">
                            <img src="<?= $logo ?>" alt="">
                        </div>
                    </div>
                    <div class="col-md-10">
                        <h1><?= ucfirst(Yii::$app->user->name) ?><span class="verified-badge"><i class="fal fa-check"></i></span></h1>
                        <div class="geodir-category-location fl-wrap">
                            <a href="#"><i class="fas fa-map-marker-alt"></i> <?= $address ?></a>
                            <a href="tel:<?= $phone ?>"> <i class="fal fa-phone"></i><?= $phone ?></a>
                            <a href="mailto:<?= $email ?>"><i class="fal fa-envelope"></i> <?= $email ?></a>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <a class="fl-wrap list-single-header-column custom-scroll-link " href="#sec5">
                        <div class="listing-rating-count-wrap single-list-count">
                            <div class="review-score">4.1</div>
                            <div class="listing-rating card-popup-rainingvis" data-starrating2="4"></div>
                            <br>
                            <div class="reviews-count">2 reviews</div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
        <div class="list-single-header_bottom fl-wrap">
            <a class="listing-item-category-wrap" href="#">
                <div class="listing-item-category  red-bg"><i class="fal fa-cheeseburger"></i></div>
                <span>Restaurants</span>
            </a>
            <div class="list-single-author"> <a href="author-single.html"><span class="author_avatar"> <img alt='' src='images/avatar/5.jpg'> </span>By Alisa Noory</a></div>
            <div class="geodir_status_date gsd_open"><i class="fal fa-lock-open"></i>Open Now</div>
            <div class="list-single-stats">
                <ul class="no-list-style">
                    <li><span class="viewed-counter"><i class="fas fa-eye"></i> Viewed - 156 </span></li>
                    <li><span class="bookmark-counter"><i class="fas fa-heart"></i> Bookmark - 24 </span></li>
                </ul>
            </div>
        </div>
    </div>
</section>
<!-- scroll-nav-wrapper-->
<div class="scroll-nav-wrapper fl-wrap">
    <div class="container">
        <nav class="scroll-nav scroll-init">
            <ul class="no-list-style">
                <li><a href='javascript:void(0)'><i class="fal fa-images"></i> Home</a></li>
                <li><a class="act-scrlink" href="javascript:void(0)"><i class="fal fa-info"></i>About</a></li>
                <li><a href="#sec3"><i class="fal fa-image"></i>Gallery</a></li>
                <li><a href="#sec5"><i class="fal fa-comments-alt"></i>Reviews</a></li>
                <li><a href="#sec5"><i class="fal fa-id-card"></i>Contact</a></li>
            </ul>
        </nav>
        <div class="scroll-nav-wrapper-opt">
            <a href="#" class="scroll-nav-wrapper-opt-btn"> <i class="fas fa-heart"></i> Save </a>
            <a href="#" class="scroll-nav-wrapper-opt-btn showshare"> <i class="fas fa-share"></i> Share </a>
            <div class="share-holder hid-share">
                <div class="share-container  isShare"></div>
            </div>
            <div class="show-more-snopt"><i class="fal fa-ellipsis-h"></i></div>
            <div class="show-more-snopt-tooltip">
                <a href="#"> <i class="fas fa-comment-alt"></i> Write a review</a>
                <a href="#"> <i class="fas fa-flag-alt"></i> Report </a>
            </div>
        </div>
    </div>
</div>
<!-- list-single-main-item-->
<div class="list-single-main-item fl-wrap block_box" id="sec3">
    <div class="list-single-main-item-title">
        <h3>Gallery / Photos</h3>
    </div>

    <div class="listing-item-grid_container fl-wrap lightgallery">
        <div class="col-sm-3" style="padding-top:15px; padding-bottom:15px;">
            <div class="box-item">
                <img src="<?= $gall_1 ?>" alt="" style="height: 190px;">
                <a href="<?= $gall_1 ?>" class="gal-link popup-image"><i class="fa fa-search"></i></a>
                <a href="javascript:void(0)" class="color-bg edit-prof_btn_right" data-tag="gall1" data-id=<?= Yii::$app->user->id ?>><i class="fal fa-edit"></i></a>
                <input id="gall1" type="file" name="gall1" style="display: none;" onchange="test()" />
            </div>
        </div>

        <div class="col-sm-3" style="padding-top:15px; padding-bottom:15px;">
            <div class="box-item">
                <img src="<?= $gall_2 ?>" alt="" style="height: 190px;">
                <a href="<?= $gall_2 ?>" class="gal-link popup-image"><i class="fa fa-search"></i></a>
                <a href="javascript:void(0)" class="color-bg edit-prof_btn_right" data-tag="gall2" data-id=<?= Yii::$app->user->id ?>><i class="fal fa-edit"></i></a>
                <input id="gall2" type="file" name="gall2" style="display: none;" onchange="test()" />
            </div>
        </div>

        <div class="col-sm-3" style="padding-top:15px; padding-bottom:15px;">
            <div class="box-item">
                <img src="<?= $gall_3 ?>" alt="" style="height: 190px;">
                <a href="<?= $gall_3 ?>" class="gal-link popup-image"><i class="fa fa-search"></i></a>
                <a href="javascript:void(0)" class="color-bg edit-prof_btn_right" data-tag="gall3" data-id=<?= Yii::$app->user->id ?>><i class="fal fa-edit"></i></a>
                <input id="gall3" type="file" name="gall3" style="display: none;" onchange="test()" />
            </div>
        </div>

        <div class="col-sm-3" style="padding-top:15px; padding-bottom:15px;">
            <div class="box-item">
                <img src="<?= $gall_4 ?>" alt="" style="height: 190px;">
                <a href="<?= $gall_4 ?>" class="gal-link popup-image"><i class="fa fa-search"></i></a>
                <a href="javascript:void(0)" class="color-bg edit-prof_btn_right" data-tag="gall4" data-id=<?= Yii::$app->user->id ?>><i class="fal fa-edit"></i></a>
                <input id="gall4" type="file" name="gall4" style="display: none;" onchange="test()" />
            </div>
        </div>

        <div class="col-sm-3" style="padding-top:15px; padding-bottom:15px;">
            <div class="box-item">
                <img src="<?= $gall_5 ?>" alt="" style="height: 190px;">
                <a href="<?= $gall_5 ?>" class="gal-link popup-image"><i class="fa fa-search"></i></a>
                <a href="javascript:void(0)" class="color-bg edit-prof_btn_right" data-tag="gall5" data-id=<?= Yii::$app->user->id ?>><i class="fal fa-edit"></i></a>
                <input id="gall5" type="file" name="gall5" style="display: none;" onchange="test()" />
            </div>
        </div>

        <div class="col-sm-3" style="padding-top:15px; padding-bottom:15px;">
            <div class="box-item">
                <img src="<?= $gall_6 ?>" alt="" style="height: 190px;">
                <a href="<?= $gall_6 ?>" class="gal-link popup-image"><i class="fa fa-search"></i></a>
                <a href="javascript:void(0)" class="color-bg edit-prof_btn_right" data-tag="gall6" data-id=<?= Yii::$app->user->id ?>><i class="fal fa-edit"></i></a>
                <input id="gall6" type="file" name="gall6" style="display: none;" onchange="test()" />
            </div>
        </div>

        <div class="col-sm-3" style="padding-top:15px; padding-bottom:15px;">
            <div class="box-item">
                <img src="<?= $gall_7 ?>" alt="" style="height: 190px;">
                <a href="<?= $gall_7 ?>" class="gal-link popup-image"><i class="fa fa-search"></i></a>
                <a href="javascript:void(0)" class="color-bg edit-prof_btn_right" data-tag="gall7" data-id=<?= Yii::$app->user->id ?>><i class="fal fa-edit"></i></a>
                <input id="gall7" type="file" name="gall7" style="display: none;" onchange="test()" />
            </div>
        </div>

        <div class="col-sm-3" style="padding-top:15px; padding-bottom:15px;">
            <div class="box-item">
                <img src="<?= $gall_8 ?>" alt="" style="height: 190px;">
                <a href="<?= $gall_8 ?>" class="gal-link popup-image"><i class="fa fa-search"></i></a>
                <a href="javascript:void(0)" class="color-bg edit-prof_btn_right" data-tag="gall8" data-id=<?= Yii::$app->user->id ?>><i class="fal fa-edit"></i></a>
                <input id="gall8" type="file" name="gall8" style="display: none;" onchange="test()" />
            </div>
        </div>
    </div>
</div>
<!-- list-single-main-item end -->