<?php
/* @var $this yii\web\View */
/* @var $model frontend\models\Vendor */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->params['breadcrumbs'][] = ['label' => 'Vendors', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$slider1 = URL::Home(true) . "theme/images/all/48.jpg";
$gall1 = URL::Home(true) . "theme/images/all/21.jpg";
$gall2 = URL::Home(true) . "theme/images/all/21.jpg";
$gall3 = URL::Home(true) . "theme/images/all/21.jpg";
$gall4 = URL::Home(true) . "theme/images/all/21.jpg";

$style1 = '';
$style2 = '';
$url = '';
if ($model->header) {
    $style2 = 'display:none';
    $url = URL::Home(true) . "theme/user/" . Yii::$app->user->name . "/" . $model->header;
} else {
    $style1 = 'display:none';
}

if ($model->slider1) {
    $slider1 = URL::Home(true) . "theme/user/" . Yii::$app->user->name . "/" . $model->slider1;
}

if ($model->gall1) {
    $gall1 = URL::Home(true) . "theme/user/" . Yii::$app->user->name . "/" . $model->gall1;
}

if ($model->gall2) {
    $gall2 = URL::Home(true) . "theme/user/" . Yii::$app->user->name . "/" . $model->gall2;
}

if ($model->gall3) {
    $gall3 = URL::Home(true) . "theme/user/" . Yii::$app->user->name . "/" . $model->gall3;
}

if ($model->gall4) {
    $gall4 = URL::Home(true) . "theme/user/" . Yii::$app->user->name . "/" . $model->gall4;
}

$address = wordwrap(Yii::$app->user->vendor()->address, 40, " <br>", TRUE);
$phone = (Yii::$app->user->vendor()->phone_alt) ? Yii::$app->user->vendor()->phone_alt : Yii::$app->user->vendor()->phone;
$email = ucfirst((Yii::$app->user->vendor()->email_alt) ? Yii::$app->user->vendor()->email_alt : Yii::$app->user->email);
$website = Yii::$app->user->vendor()->website;
$fb = (Yii::$app->user->vendor()->facebook) ? Yii::$app->user->vendor()->facebook : "#";
$insta = (Yii::$app->user->vendor()->instagram) ?  Yii::$app->user->vendor()->instagram : "#";
$twit = (Yii::$app->user->vendor()->twitter) ? Yii::$app->user->vendor()->twitter : "#";

$logo  = URL::Home(true) . "theme/images/avatar/4.jpg";
if (Yii::$app->user->logo) {
    $logo = URL::Home(true) . "theme/user/" . Yii::$app->user->name . "/" . Yii::$app->user->logo;
}
?>
<script src="<?= Url::home(true); ?>theme/ckeditor/ckeditor.js"></script>
<script src="<?= Url::home(true); ?>theme/ckeditor/samples/js/sample.js"></script>
<div class="content">
    <section class="listing-hero-section hidden-section" data-scrollax-parent="true" id="sec1">

        <div class="bg-parallax-wrap" style="<?= $style1 ?>" id='final_i'>
            <div class="bg par-elem parth" data-bg="<?= $url ?>" data-scrollax="properties: { translateY: '30%' }"></div>
            <div class="overlay"></div>

        </div>
        <div class="bg-parallax-wrap gradient-bg" style="<?= $style2 ?>" id='temp_i'>
            <div class="gradient-bg-figure" style="right:-30px;top:10px;"></div>
            <div class="gradient-bg-figure" style="left:-20px;bottom:30px;"></div>
            <div class="circle-wrap" style="left:120px;bottom:120px;" data-scrollax="properties: { translateY: '-200px' }">
                <div class="circle_bg-bal circle_bg-bal_small"></div>
            </div>
            <div class="circle-wrap" style="right:420px;bottom:-70px;" data-scrollax="properties: { translateY: '150px' }">
                <div class="circle_bg-bal circle_bg-bal_big"></div>
            </div>
            <div class="circle-wrap" style="left:420px;top:-70px;" data-scrollax="properties: { translateY: '100px' }">
                <div class="circle_bg-bal circle_bg-bal_big"></div>
            </div>
            <div class="circle-wrap" style="left:40%;bottom:-70px;">
                <div class="circle_bg-bal circle_bg-bal_middle"></div>
            </div>
            <div class="circle-wrap" style="right:40%;top:-10px;">
                <div class="circle_bg-bal circle_bg-bal_versmall" data-scrollax="properties: { translateY: '-350px' }"></div>
            </div>
            <div class="circle-wrap" style="right:55%;top:90px;">
                <div class="circle_bg-bal circle_bg-bal_versmall" data-scrollax="properties: { translateY: '-350px' }"></div>
            </div>
        </div>

        <a href="javascript:void(0)" class="color-bg edit-prof_btn_right" data-tag="home_header" data-id=<?= Yii::$app->user->id ?>><i class="fal fa-edit"></i></a>
        <input id="home_header" type="file" name="home_header" style="display: none;" onchange="test()" />

        <div class="container">
            <div class="list-single-header-item  fl-wrap">
                <div class="row">

                    <div class="col-md-9">
                        <div class="col-md-2">
                            <div class="header-logo">
                                <img src="<?= $logo ?>" alt="">
                            </div>
                        </div>
                        <div class="col-md-10">
                            <h1><?= ucfirst(Yii::$app->user->name) ?><span class="verified-badge"><i class="fal fa-check"></i></span></h1>
                            <div class="geodir-category-location fl-wrap">
                                <a href="#"><i class="fas fa-map-marker-alt"></i> <?= $address ?></a>
                                <a href="tel:<?= $phone ?>"> <i class="fal fa-phone"></i><?= $phone ?></a>
                                <a href="mailto:<?= $email ?>"><i class="fal fa-envelope"></i> <?= $email ?></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <a class="fl-wrap list-single-header-column custom-scroll-link " href="#sec5">
                            <div class="listing-rating-count-wrap single-list-count">
                                <div class="review-score">4.1</div>
                                <div class="listing-rating card-popup-rainingvis" data-starrating2="4"></div>
                                <br>
                                <div class="reviews-count">2 reviews</div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            <div class="list-single-header_bottom fl-wrap">
                <a class="listing-item-category-wrap" href="#">
                    <div class="listing-item-category  red-bg"><i class="fal fa-cheeseburger"></i></div>
                    <span>Restaurants</span>
                </a>
                <div class="list-single-author"> <a href="author-single.html"><span class="author_avatar"> <img alt='' src='images/avatar/5.jpg'> </span>By Alisa Noory</a></div>
                <div class="geodir_status_date gsd_open"><i class="fal fa-lock-open"></i>Open Now</div>
                <div class="list-single-stats">
                    <ul class="no-list-style">
                        <li><span class="viewed-counter"><i class="fas fa-eye"></i> Viewed - 156 </span></li>
                        <li><span class="bookmark-counter"><i class="fas fa-heart"></i> Bookmark - 24 </span></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <!-- scroll-nav-wrapper-->
    <div class="scroll-nav-wrapper fl-wrap">
        <div class="container">
            <nav class="scroll-nav scroll-init">
                <ul class="no-list-style">
                    <li><a class="act-scrlink" href=""><i class="fal fa-images"></i> Home</a></li>
                    <li><a href="javascript:void(0)"><i class="fal fa-info"></i>About</a></li>
                    <li><a href="javascript:void(0)"><i class="fal fa-image"></i>Gallery</a></li>
                    <li><a href="javascript:void(0)"><i class="fal fa-comments-alt"></i>Reviews</a></li>
                    <li><a href="javascript:void(0)"><i class="fal fa-id-card"></i>Contact</a></li>
                </ul>
            </nav>
            <div class="scroll-nav-wrapper-opt">
                <a href="#" class="scroll-nav-wrapper-opt-btn"> <i class="fas fa-heart"></i> Save </a>
                <a href="#" class="scroll-nav-wrapper-opt-btn showshare"> <i class="fas fa-share"></i> Share </a>
                <div class="share-holder hid-share">
                    <div class="share-container  isShare"></div>
                </div>
                <div class="show-more-snopt"><i class="fal fa-ellipsis-h"></i></div>
                <div class="show-more-snopt-tooltip">
                    <a href="#"> <i class="fas fa-comment-alt"></i> Write a review</a>
                    <a href="#"> <i class="fas fa-flag-alt"></i> Report </a>
                </div>
            </div>
        </div>
    </div>

    <?php $form = ActiveForm::begin([
        'fieldConfig' => [
            'template' => "{input}\n{error}",
            'errorOptions' => ['encode' => false, 'class' => 'error-cust'],
        ],
        'options' => ['autocomplete' => 'off'],
    ]); ?>
    <!-- scroll-nav-wrapper end-->
    <section class="gray-bg no-top-padding">
        <div class="container">
            <div class="clearfix"></div>
            <div class="row">
                <!-- list-single-main-wrapper-col -->
                <div class="col-md-8">
                    <!-- list-single-main-wrapper -->
                    <div class="list-single-main-wrapper fl-wrap" id="sec2">
                        <div class="list-single-main-media fl-wrap">
                            <img src="<?= $slider1 ?>" class="respimg" alt="">
                            <a href="javascript:void(0)" class="color-bg edit-prof_btn_right" data-tag="slider1" data-id=<?= Yii::$app->user->id ?>><i class="fal fa-edit"></i></a>
                            <input id="slider1" type="file" name="slider1" style="display: none;" onchange="test()" />
                        </div>
                        <!-- list-single-main-item -->
                        <div class="list-single-main-item fl-wrap block_box">
                            <div class="list-single-main-item-title">
                                <h3>Description</h3>
                            </div>
                            <div class="list-single-main-item_content fl-wrap">
                                <?= $form->field($model, 'short_about')->textarea(['id' => 'editor'])->label(false) ?>
                                <a href="#" class="btn color2-bg float-btn">Read More<i class="fal fa-chevron-right"></i></a>
                            </div>
                        </div>
                        <!-- list-single-main-item end -->
                        <!-- list-single-main-item-->
                        <div class="list-single-main-item fl-wrap block_box" id="sec3">
                            <div class="list-single-main-item-title">
                                <h3>Gallery / Photos</h3>
                            </div>
                            <div class="list-single-main-item_content fl-wrap">
                                <div class="single-carousel-wrap fl-wrap lightgallery">
                                    <div class="sc-next sc-btn color2-bg"><i class="fas fa-caret-right"></i></div>
                                    <div class="sc-prev sc-btn color2-bg"><i class="fas fa-caret-left"></i></div>
                                    <div class="single-carousel fl-wrap full-height">
                                        <div class="swiper-container">
                                            <div class="swiper-wrapper">
                                                <!-- swiper-slide-->
                                                <div class="swiper-slide">
                                                    <div class="box-item" id='vgall1'>
                                                        <img src="<?= $gall1 ?>" alt="">
                                                        <a href="<?= $gall1 ?>" class="gal-link popup-image"><i class="fa fa-search"></i></a>
                                                        <a href="javascript:void(0)" class="color-bg edit-prof_btn_right" data-tag="gall1" data-id=<?= Yii::$app->user->id ?>><i class="fal fa-edit"></i></a>
                                                        <input id="gall1" type="file" name="gall1" style="display: none;" onchange="test()" />
                                                    </div>
                                                </div>
                                                <!-- swiper-slide end-->
                                                <!-- swiper-slide-->
                                                <div class="swiper-slide">
                                                    <div class="box-item" id='vgall2'>
                                                        <img src="<?= $gall2 ?>" alt="">
                                                        <a href="<?= $gall2 ?>" class="gal-link popup-image"><i class="fa fa-search"></i></a>
                                                        <a href="javascript:void(0)" class="color-bg edit-prof_btn_right" data-tag="gall2" data-id=<?= Yii::$app->user->id ?>><i class="fal fa-edit"></i></a>
                                                        <input id="gall2" type="file" name="gall2" style="display: none;" onchange="test()" />
                                                    </div>
                                                </div>
                                                <!-- swiper-slide end-->
                                                <!-- swiper-slide-->
                                                <div class="swiper-slide">
                                                    <div class="box-item" id='vgall3'>
                                                        <img src="<?= $gall3 ?>" alt="">
                                                        <a href="<?= $gall3 ?>" class="gal-link popup-image"><i class="fa fa-search"></i></a>
                                                        <a href="javascript:void(0)" class="color-bg edit-prof_btn_right" data-tag="gall3" data-id=<?= Yii::$app->user->id ?>><i class="fal fa-edit"></i></a>
                                                        <input id="gall3" type="file" name="gall3" style="display: none;" onchange="test()" />
                                                    </div>
                                                </div>
                                                <!-- swiper-slide end-->
                                                <!-- swiper-slide-->
                                                <div class="swiper-slide">
                                                    <div class="box-item" id='vgall4'>
                                                        <img src="<?= $gall4 ?>" alt="">
                                                        <a href="<?= $gall4 ?>" class="gal-link popup-image"><i class="fa fa-search"></i></a>
                                                        <a href="javascript:void(0)" class="color-bg edit-prof_btn_right" data-tag="gall4" data-id=<?= Yii::$app->user->id ?>><i class="fal fa-edit"></i></a>
                                                        <input id="gall4" type="file" name="gall4" style="display: none;" onchange="test()" />
                                                    </div>
                                                </div>
                                                <!-- swiper-slide end-->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- list-single-main-item end -->
                    </div>
                </div>
                <!-- list-single-main-wrapper-col end -->
                <!-- list-single-sidebar -->
                <div class="col-md-4">
                    <!--box-widget-item -->
                    <div class="box-widget-item fl-wrap block_box">
                        <div class="box-widget-item-header">
                            <h3>Working Hours</h3>
                        </div>
                        <div class="box-widget opening-hours fl-wrap">
                            <div class="box-widget-content">
                                <ul class="no-list-style">
                                    <li class="mon">
                                        <span class="opening-hours-day">Monday </span>
                                        <span class="opening-hours-time"><?= $form->field($model, 'mon')->textInput(['class' => 'form-control']) ?></span>
                                    </li>
                                    <li class="tue"><span class="opening-hours-day">Tuesday </span><span class="opening-hours-time"><?= $form->field($model, 'tue')->textInput(['class' => 'form-control', 'onchange' => 'textSave("home")']) ?></span></li>
                                    <li class="wed"><span class="opening-hours-day">Wednesday </span><span class="opening-hours-time"><?= $form->field($model, 'wed')->textInput(['class' => 'form-control', 'onchange' => 'textSave("home")']) ?></span></li>
                                    <li class="thu"><span class="opening-hours-day">Thursday </span><span class="opening-hours-time"><?= $form->field($model, 'thu')->textInput(['class' => 'form-control', 'onchange' => 'textSave("home")']) ?></span></li>
                                    <li class="fri"><span class="opening-hours-day">Friday </span><span class="opening-hours-time"><?= $form->field($model, 'fri')->textInput(['class' => 'form-control', 'onchange' => 'textSave("home")']) ?></span></li>
                                    <li class="sat"><span class="opening-hours-day">Saturday </span><span class="opening-hours-time"><?= $form->field($model, 'sat')->textInput(['class' => 'form-control', 'onchange' => 'textSave("home")']) ?></span></li>
                                    <li class="sun"><span class="opening-hours-day">Sunday </span><span class="opening-hours-time"><?= $form->field($model, 'sun')->textInput(['class' => 'form-control', 'onchange' => 'textSave("home")']) ?></span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!--box-widget-item end -->
                    <!--box-widget-item -->
                    <div class="box-widget-item fl-wrap block_box">
                        <div class="box-widget-item-header">
                            <h3>Location / Contacts </h3>
                        </div>
                        <div class="box-widget">
                            <div class="map-container">
                                <div id="singleMap" data-latitude="40.7427837" data-longitude="-73.11445617675781" data-mapTitle="Our Location"></div>
                            </div>
                            <div class="box-widget-content bwc-nopad">
                                <div class="list-author-widget-contacts list-item-widget-contacts bwc-padside">
                                    <ul class="no-list-style">
                                        <li><span><i class="fal fa-map-marker"></i> Adress :</span> <a href="#"><?= wordwrap(Yii::$app->user->vendor()->address, 40, " <br>", TRUE); ?></a></li>
                                        <li><span><i class="fal fa-phone"></i> Phone :</span> <a href="#"><?= (Yii::$app->user->vendor()->phone_alt) ? Yii::$app->user->vendor()->phone_alt : Yii::$app->user->vendor()->phone ?></a></li>
                                        <li><span><i class="fal fa-envelope"></i> Mail :</span> <a href="#"><?= (Yii::$app->user->vendor()->phone_alt) ? Yii::$app->user->vendor()->email_alt : Yii::$app->user->email ?></a></li>
                                        <li><span><i class="fal fa-browser"></i> Website :</span> <a href="#"><?= Yii::$app->user->vendor()->website ?></a></li>
                                    </ul>
                                </div>
                                <div class="list-widget-social bottom-bcw-box  fl-wrap">
                                    <ul class="no-list-style">
                                        <li><a href="<?=$fb?>" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                                        <li><a href="<?=$insta?>" target="_blank"><i class="fab fa-twitter"></i></a></li>
                                        <li><a href="<?=$twit?>" target="_blank"><i class="fab fa-instagram"></i></a></li>
                                    </ul>
                                    <div class="bottom-bcw-box_link"><a href="#" class="show-single-contactform tolt" data-microtip-position="top" data-tooltip="Write Message"><i class="fal fa-envelope"></i></a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--box-widget-item end -->
                </div>
                <!-- list-single-sidebar end -->
            </div>
        </div>

    </section>
    <?= Html::submitButton('Save <i class="fal fa-save"></i>', ['class' => 'btn color2-bg to-top-custom', 'style' => 'display:none']) ?>
    <?php ActiveForm::end(); ?>
    <div class="limit-box fl-wrap"></div>
</div>

<script>
    initSample();
</script>